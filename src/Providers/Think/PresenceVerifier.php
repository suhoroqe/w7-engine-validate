<?php

namespace W7\Validate\Providers\Think;

use Itwmw\Validation\Support\Interfaces\PresenceVerifierInterface;
use think\facade\Db;

class PresenceVerifier implements PresenceVerifierInterface
{
    protected $db;

    public function __construct()
    {
        $this->db = Db::newQuery();
    }

    public function setConnection(?string $connection)
    {
        if (!empty($connection)) {
            $this->db->connect($connection);
        }
    }

    public function table(string $table): PresenceVerifierInterface
    {
        $this->db->table($this->db->getTable($table));
        return $this;
    }

    public function where(string $column, $operator = null, $value = null, string $boolean = 'and'): PresenceVerifierInterface
    {
        switch (strtolower($boolean)) {
            case 'and':
                $this->db->where($column, $operator, $value);
                break;
            case 'or':
                $this->db->whereOr($column, $operator, $value);
                break;
            case 'xor':
                $this->db->whereXor($column, $operator, $value);
                break;
            default:
                throw new \RuntimeException('操作错误');
        }

        return $this;
    }

    public function whereIn(string $column, $values, string $boolean = 'and'): PresenceVerifierInterface
    {
        $this->db->whereIn($column, $values, $boolean);
        return $this;
    }

    public function count(string $columns = '*'): int
    {
        return $this->db->count($columns);
    }

    public function newQuery()
    {
        $this->db = $this->db->newQuery();
    }
}
