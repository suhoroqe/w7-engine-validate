<?php

namespace W7\Tests\Test;

use W7\Tests\Material\BaseTestValidate;
use W7\Validate\Exception\ValidateException;
use W7\Validate\Validate;

class TestArrayRule extends BaseTestValidate
{
    /**
     * @test 测试指定字段是一个数组
     */
    public function testIsArray()
    {
        $v = new class extends Validate {
            protected $rule = [
                'a' => 'array'
            ];
        };

        $data = [
            'a' => [1, 2]
        ];
        $result = $v->check($data);

        $this->assertEquals($data, $result);
        $this->expectException(ValidateException::class);
        $this->expectExceptionMessageMatches('/^A 必须是一个数组。/');
        $v->check([
            'a' => 1
        ]);
    }

    /**
     * @test 测试指定字段是一个数字索引的数组
     */
    public function testArrayIsList()
    {
        $v = new class extends Validate {
            protected $rule = [
                'a' => 'array:@keyInt'
            ];
        };

        $data = [
            'a' => [1, 2, 3]
        ];

        $result = $v->check($data);

        $this->assertEquals($data, $result);

        $this->expectException(ValidateException::class);
        $this->expectExceptionMessageMatches('/^A 必须是一个索引为数字且连续的数组。/');
        $v->check([
            'a' => [
                's' => 1
            ]
        ]);
    }

    /**
     * @test 测试指定字段包含指定的字段
     */
    public function testArrayRequireKeys()
    {
        $v = new class extends Validate {
            protected $rule = [
                'a' => 'array:a,b,c'
            ];

            protected $customAttributes = [
                'a.a' => 'A元素',
                'a.b' => 'B元素',
            ];
        };

        $data = [
            'a' => [
                'a' => 1,
                'b' => 1,
                'c' => 1,
            ]
        ];

        $result = $v->check($data);

        $this->assertEquals($data, $result);
        $this->expectException(ValidateException::class);
        $this->expectExceptionMessageMatches('/^A 必须包含成员 A元素,B元素,c/');
        $v->check([
            'a' => [
                's' => 1
            ]
        ]);
    }

    /**
     * @test 测试数组只能包含指定的字段
     */
    public function testArrayOptionalKeys()
    {
        $v = new class extends Validate {
            protected $rule = [
                'a' => 'array:a,b,c,[d],[e]'
            ];

            protected $customAttributes = [
                'a'   => '测试数组',
                'a.a' => 'A元素',
                'a.b' => 'B元素',
            ];
        };

        $data = [
            'a' => [
                'a' => 1,
                'b' => 1,
                'c' => 1,
                'e' => 5
            ]
        ];

        $result = $v->check($data);

        $this->assertEquals($data, $result);
        $this->expectException(ValidateException::class);
        $this->expectExceptionMessageMatches('/^测试数组 只能存在成员 d,e,A元素,B元素,c/');
        $v->check([
            'a' => [
                'a' => 1,
                'b' => 1,
                'c' => 1,
                'f' => 5
            ]
        ]);
    }
}
