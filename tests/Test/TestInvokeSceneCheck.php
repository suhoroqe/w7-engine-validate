<?php

namespace W7\Tests\Test;

use W7\Tests\Material\BaseTestValidate;
use W7\Tests\Material\Queue;
use W7\Validate\Exception\ValidateException;
use W7\Validate\Support\Event\ValidateEventAbstract;
use W7\Validate\Support\Processor\ProcessorExecCond;
use W7\Validate\Support\Processor\ProcessorParams;
use W7\Validate\Support\ValidateScene;
use W7\Validate\Validate;

class TestInvokeSceneCheckEvent extends ValidateEventAbstract
{
    public function beforeValidate(): bool
    {
        Queue::push('testInvokeSceneCheckEvent', 'eventClassBefore');
        return true;
    }

    public function afterValidate(): bool
    {
        Queue::push('testInvokeSceneCheckEvent', 'eventClassAfter');
        return true;
    }
}

class TestInvokeSceneCheckInSceneEvent extends ValidateEventAbstract
{
    public function beforeValidate(): bool
    {
        Queue::push('testInvokeSceneCheckEvent', 'eventInSceneClassBefore');
        return true;
    }

    public function afterValidate(): bool
    {
        Queue::push('testInvokeSceneCheckEvent', 'eventInSceneClassAfter');
        return true;
    }
}

class TestInvokeSceneCheck extends BaseTestValidate
{
    /**
     * @test 测试当验证器本身存在规则时，通过闭包调用场景
     *
     * @throws \W7\Validate\Exception\ValidateException
     */
    public function testHasRule()
    {
        $v = new class extends Validate {
            protected $rule = [
                'a' => 'required',
                'b' => 'required'
            ];
        };

        $input = [
            'a' => '123',
            'b' => '456',
            'c' => '556'
        ];

        $data = $v->invokeSceneCheck(function (ValidateScene $scene) {
            $scene->only(['a']);
        }, $input);

        $this->assertSame('123', $data['a']);
        $this->assertCount(1, $data);
    }

    /**
     * @test 测试闭包场景中定义预处理器
     *
     * @throws \W7\Validate\Exception\ValidateException
     */
    public function testPreprocessor()
    {
        $data = Validate::make([
            'a' => 'required'
        ])->invokeSceneCheck(function (ValidateScene $scene) {
            $scene->only(['a'])
                ->preprocessor('a', '123');
        }, [
            'c' => 1
        ]);

        $this->assertSame('123', $data['a']);
        $this->assertCount(1, $data);
    }

    /**
     * @test 测试在场景闭包中使用过滤器
     *
     * @throws \W7\Validate\Exception\ValidateException
     */
    public function testFiller()
    {
        $data = Validate::make()->invokeSceneCheck(function (ValidateScene $scene) {
            $scene->only(['a', 'a.*'])
                ->append('a', 'array')
                ->postprocessor('a.*', 'intval', ProcessorExecCond::WHEN_NOT_EMPTY, ProcessorParams::Value);
        }, [
            'a' => [
                '123',
                true,
                1
            ]
        ]);

        $this->assertSame(123, $data['a'][0]);
        $this->assertSame(1, $data['a'][1]);
        $this->assertSame(1, $data['a'][2]);
    }

    /**
     * @test 测试错误消息
     *
     * @throws ValidateException
     */
    public function testErrorMessage()
    {
        $this->expectException(ValidateException::class);
        $this->expectExceptionMessageMatches('/^Test A Error Message$/');
        Validate::make([], [
            'a.required' => 'Test A Error Message'
        ])->invokeSceneCheck(function (ValidateScene $scene) {
            $scene->appendCheckField('a')->append('a', 'required');
        }, []);
    }

    /**
     * @test 测试闭包场景验证中的Next，以及返回合格数据的顺序是否符合预期
     *
     * @throws ValidateException
     */
    public function testSceneNext()
    {
        $v = new class extends Validate {
            protected $rule = [
                'a' => 'required',
                'b' => 'required'
            ];

            protected $scene = [
                'checkA' => ['a', 'next' => 'checkB'],
                'checkB' => ['b']
            ];
        };

        $data = $v->invokeSceneCheck(function (ValidateScene $scene) {
            $scene->only(['c'])->next('checkA');
        }, [
            'a' => 123,
            'b' => 123,
            'c' => 123,
            'd' => 123
        ]);

        $this->assertEquals(['c', 'a', 'b'], array_keys($data));
    }

    /**
     * @test 测试闭包场景事件以及执行顺序
     *
     * @throws ValidateException
     */
    public function testEvent()
    {
        $v = new class extends Validate {
            protected $event = TestInvokeSceneCheckEvent::class;

            protected $scene = [
                'testSceneEvent'    => ['event' => TestInvokeSceneCheckInSceneEvent::class, 'next' => 'testScentFunction'],
                'testScentFunction' => ['before' => 'test', 'after' => 'test']
            ];

            protected function beforeTest(): bool
            {
                Queue::push('testInvokeSceneCheckEvent', 'eventInSceneFunctionBefore');
                return true;
            }

            protected function afterTest(): bool
            {
                Queue::push('testInvokeSceneCheckEvent', 'eventInSceneFunctionAfter');
                return true;
            }
        };

        $v->invokeSceneCheck(function (ValidateScene $scene) {
            $scene->after(function () {
                Queue::push('testInvokeSceneCheckEvent', 'eventInSceneClosureAfter');
                return true;
            })->before(function () {
                Queue::push('testInvokeSceneCheckEvent', 'eventInSceneClosureBefore');
                return true;
            })->next('testSceneEvent');
        }, []);

        $this->assertSame(8, Queue::count('testInvokeSceneCheckEvent'));
        $this->assertSame('eventClassBefore', Queue::shift('testInvokeSceneCheckEvent'));
        $this->assertSame('eventInSceneClosureBefore', Queue::shift('testInvokeSceneCheckEvent'));
        $this->assertSame('eventInSceneClosureAfter', Queue::shift('testInvokeSceneCheckEvent'));
        $this->assertSame('eventInSceneClassBefore', Queue::shift('testInvokeSceneCheckEvent'));
        $this->assertSame('eventInSceneClassAfter', Queue::shift('testInvokeSceneCheckEvent'));
        $this->assertSame('eventInSceneFunctionBefore', Queue::shift('testInvokeSceneCheckEvent'));
        $this->assertSame('eventInSceneFunctionAfter', Queue::shift('testInvokeSceneCheckEvent'));
        $this->assertSame('eventClassAfter', Queue::shift('testInvokeSceneCheckEvent'));
        $this->assertSame(0, Queue::count('testInvokeSceneCheckEvent'));
    }
}
