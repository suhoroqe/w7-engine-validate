<?php

namespace W7\Tests\Test;

use W7\Tests\Material\BaseTestValidate;
use W7\Validate\Exception\ValidateException;
use W7\Validate\Validate;

class ValidateSceneIsSceneClass extends Validate
{
    protected $rule = [
        'name' => 'required',
        'age'  => 'required|integer',
    ];

    public function __construct()
    {
        $scene = $this->makeValidateScene();
        $scene->only(['name'])
            ->after(function (array $data) {
                if ('tom' != $data['name']) {
                    return 'name must be tom';
                }
                return true;
            });

        $this->setScene([
            'test' => $scene
        ]);
    }
}

class TestValidateSceneIsSceneClass extends BaseTestValidate
{
    public function testValidateSceneIsSceneClass()
    {
        $input = [
            'name' => 'tom',
            'age'  => 18,
        ];

        $validator = new ValidateSceneIsSceneClass();
        $validator->scene('test');
        $data = $validator->check($input);

        $this->assertSame($input['name'], $data['name']);
        $this->assertArrayNotHasKey('age', $data);
    }

    public function testNameNotIsTom()
    {
        $this->expectException(ValidateException::class);
        $this->expectExceptionMessage('name must be tom');
        $input = [
            'name' => 'toms',
        ];

        $validator = new ValidateSceneIsSceneClass();
        $validator->scene('test');
        $validator->check($input);
    }
}
