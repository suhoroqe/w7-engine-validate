<?php

namespace W7\Tests\Test;

use W7\Tests\Material\BaseTestValidate;
use W7\Validate\Support\ValidateScene;
use W7\Validate\Validate;

class TestRuleGroup extends BaseTestValidate
{
    /**
     * @test 测试在规则中使用规则组
     *
     * @return void
     */
    public function testGroupForRule()
    {
        $v = new class extends Validate {
            protected $group = [
                'r'        => 'required',
                'username' => 'alpha_num|min:5|max:10'
            ];

            protected $rule = [
                'username' => 'r|username|string'
            ];
        };
        $this->assertEquals([
            'username' => ['required', 'alpha_num', 'min:5', 'max:10', 'string']
        ], $v->getCheckRules());
    }

    /**
     * 测试在自定义验证场景中使用规则组.
     *
     * @return void
     */
    public function testGroupForScene()
    {
        $v = new class extends Validate {
            protected $group = [
                'r'        => 'required',
                'username' => 'alpha_num|min:5|max:10'
            ];

            protected function sceneTest(ValidateScene $scene)
            {
                $scene->only(['username'])
                    ->append('username', 'r|username|string');
            }
        };

        $this->assertEquals([
            'username' => ['required', 'alpha_num', 'min:5', 'max:10', 'string']
        ], $v->scene('test')->getCheckRules());
    }
}
